//
//  EvasView.m
//  evas_quartz_test
//
//  Created by Tim Horton on 2008.06.01.
//  Copyright 2008 Tim Horton. All rights reserved.
//

#import "EvasView.h"
#import "Evas.h"
#import "Evas_Engine_Quartz.h"
#import "evas_test_main.h"
#include <sys/param.h>

@implementation EvasView

void SetupBundle()
{
	ProcessSerialNumber psn;
	FSRef location;
	GetCurrentProcess(&psn);
	GetProcessBundleLocation(&psn, &location);
	CFURLRef theURL = CFURLCreateFromFSRef(kCFAllocatorDefault, &location);
	char outPathName[MAXPATHLEN];
	CFURLGetFileSystemRepresentation(theURL, true, (UInt8*)outPathName, MAXPATHLEN);
	CFRelease(theURL);
	strcat(outPathName, "/Contents/Resources");
	chdir(outPathName);
}

- (id) init
{
	self = [super init];
	if (self != nil)
	{
		ctx = NULL;
		count = 1;
	}
	return self;
}

- (void)drawFrame:(id)stuff
{
	if(running)
		loop();
	
	evas_render(evas);
	
#if VIDEO_OUTPUT
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	CGImageRef cgim = CGBitmapContextCreateImage(ctx);
	NSBitmapImageRep *bitmapRep = [[NSBitmapImageRep alloc] initWithCGImage:cgim];
	NSData * bitdat = [bitmapRep representationUsingType:NSPNGFileType properties:[NSDictionary dictionaryWithObject:[NSDecimalNumber numberWithFloat:0.9] forKey:NSImageCompressionFactor]];
	[bitdat writeToFile:[NSString stringWithFormat:@"/Users/hortont/Desktop/video/%.6i.png",count] atomically:YES];
	[bitmapRep release];
	CGImageRelease(cgim);
	++count;
	[pool release];
#endif
}

- (void)drawRect:(NSRect)rect
{
	if(ctx != NULL)
	{
		[self setNeedsDisplay:YES];
		return;
	}
	
	SetupBundle();
	
#if VIDEO_OUTPUT
	int bitmapBytesPerRow = (win_w * 4);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	void * bitmapData = calloc(win_h, bitmapBytesPerRow);
	
	ctx = CGBitmapContextCreate (bitmapData, win_w, win_h, 8, bitmapBytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast);
	CGColorSpaceRelease(colorSpace);
#else
	ctx = [[NSGraphicsContext currentContext] graphicsPort];
#endif
	
	CGContextRetain(ctx);
	
	evas_init();
	evas = evas_new();
	evas_output_method_set(evas, evas_render_method_lookup("quartz"));
	evas_output_size_set(evas, win_w, win_h);
	evas_output_viewport_set(evas, 0, 0, win_w, win_h);
	
	Evas_Engine_Info_Quartz *einfo;
	
	einfo = (Evas_Engine_Info_Quartz *) evas_engine_info_get(evas);
	
	einfo->info.context = ctx;
	
	evas_engine_info_set(evas, (Evas_Engine_Info *) einfo);
	
	setup();
	
	orig_start_time = start_time = get_time();
	
	running = YES;
	
	[NSTimer scheduledTimerWithTimeInterval:.0166 target:self selector:@selector(drawFrame:) userInfo:nil repeats:YES];
}

- (void)mouseDown:(NSEvent *)event
{
	running = !running;
	
	if(running)
		start_time += (get_time() - pause_time);
	else
		pause_time = get_time();
}

@end
