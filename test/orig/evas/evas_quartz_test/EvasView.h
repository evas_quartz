//
//  EvasView.h
//  evas_quartz_test
//
//  Created by Tim Horton on 2008.06.01.
//  Copyright 2008 Tim Horton. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EvasView : NSView
{
	CGContextRef ctx;
	NSImage * img;
	int count;
	bool running;
	double pause_time;
}

@end
