//
//  main.m
//  evas_quartz_test
//
//  Created by Tim Horton on 2008.06.01.
//  Copyright Tim Horton 2008. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
