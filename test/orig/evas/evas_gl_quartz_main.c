#include "evas_test_main.h"

#include <sys/param.h>
#include <unistd.h>

#include <Carbon/Carbon.h>
#include <OpenGL/OpenGL.h>
#include <AGL/agl.h>

#include "Evas.h"
#include "Evas_Engine_GL_Quartz.h"

void drawGL (WindowRef window, Boolean swap)
{
	loop();
	evas_render(evas);
}

void resizeWindow(WindowRef window, AGLContext aglContext)
{
   // this is NOT WORKING. why!?
   Rect bounds;
   GetWindowPortBounds(window, &bounds);
   aglUpdateContext(aglGetCurrentContext());
   glViewport(0, 0, bounds.right-bounds.left, bounds.bottom-bounds.top);
   evas_output_size_set(evas, bounds.right-bounds.left, bounds.bottom-bounds.top);
   evas_output_viewport_set(evas, 0, 0, bounds.right-bounds.left, bounds.bottom-bounds.top);
}

static OSStatus handleKeyInput (EventHandlerCallRef myHandler, EventRef event, Boolean keyDown, void* userData)
{
	WindowRef window = (WindowRef) userData;

	OSStatus result = CallNextEventHandler(myHandler, event);
	
	if (eventNotHandledErr == result)
	{ 
		char character;
		GetEventParameter (event, kEventParamKeyMacCharCodes, typeChar, NULL, sizeof(char), NULL, &character);
		
		if (keyDown)
		{
			switch (character)
			{
				case 'q':
               result = noErr;
               exit(0);
					break;
			}
		}
	}
	
	return result;
}

static OSStatus windowEvtHndlr(EventHandlerCallRef myHandler,
                                       EventRef event,
                                       void* userData)
{
   OSStatus    result = eventNotHandledErr;
   WindowRef     window;
   AGLContext    aglContext = aglGetCurrentContext();

   GetEventParameter(event, kEventParamDirectObject, typeWindowRef,
                     NULL, sizeof(WindowRef), NULL, &window);

   if(GetEventClass(event) == kEventClassWindow)
   {
      switch(GetEventKind(event))
      {
         case kEventWindowClose:
            HideWindow (window);
            result = noErr;
            break;
         case kEventWindowBoundsChanged:
            resizeWindow(window, aglContext);
            result = noErr;
            break;
         case kEventWindowZoomed:
            resizeWindow(window, aglContext);
            result = noErr;
            break;
      }
   }
   else if(GetEventClass(event) == kEventClassKeyboard)
   {
      switch (GetEventKind(event))
      {
         case kEventRawKeyDown:
            result = handleKeyInput(myHandler, event, true, userData);
            break;
         case kEventRawKeyUp:
            result = handleKeyInput(myHandler, event, false, userData);
            break;
      }
   }

  return result;
}

static void timerContextCB (WindowRef window)
{
	drawGL (window, true); // required to do this directly to get animation during resize and drags
}

static void timerCB (EventLoopTimerRef inTimer, void* userData)
{
   timerContextCB ((WindowRef) userData); // timer based update
}

void SetupBundle()
{
   ProcessSerialNumber psn;
   FSRef location;
   GetCurrentProcess(&psn);
   GetProcessBundleLocation(&psn, &location);
   CFURLRef theURL = CFURLCreateFromFSRef(kCFAllocatorDefault, &location);
   char outPathName[MAXPATHLEN];
   CFURLGetFileSystemRepresentation(theURL, true, (UInt8*)outPathName, MAXPATHLEN);
   CFRelease(theURL);
   strcat(outPathName, "/Contents");
   chdir(outPathName);
}

int
main(int argc, char **argv)
{
   SetupBundle();

   WindowRef     window;

   /* test evas_free....  :) */
   evas_init();
   evas = evas_new();
   evas_output_method_set(evas, evas_render_method_lookup("gl_quartz"));
   evas_output_size_set(evas, win_w, win_h);
   evas_output_viewport_set(evas, 0, 0, win_w, win_h);
  
   Evas_Engine_Info_GL_Quartz *einfo;

   einfo = (Evas_Engine_Info_GL_Quartz *) evas_engine_info_get(evas);

   CreateNewWindow(kDocumentWindowClass, kWindowStandardDocumentAttributes | kWindowStandardHandlerAttribute | kWindowCompositingAttribute, &((Rect){ 100, 100, win_h + 100, win_w + 100 }), &window);
   EventHandlerUPP gWinEvtHandler = NewEventHandlerUPP(windowEvtHndlr); 
	
	EventLoopTimerUPP sTimerUPP = NewEventLoopTimerUPP (timerCB);
	EventLoopTimerRef timer;
	
	EventHandlerRef ref;
   EventTypeSpec list[] = { { kEventClassWindow, kEventWindowCollapsing }, { kEventClassWindow, kEventWindowShown }, { kEventClassWindow, kEventWindowActivated }, { kEventClassWindow, kEventWindowClose }, { kEventClassWindow, kEventWindowDrawContent }, { kEventClassWindow, kEventWindowBoundsChanged }, { kEventClassWindow, kEventWindowZoomed }, { kEventClassKeyboard, kEventRawKeyDown }, { kEventClassKeyboard, kEventRawKeyUp } };

	InstallWindowEventHandler (window, gWinEvtHandler, GetEventTypeCount (list), list, (void*)window, &ref); // add event handler	
	InstallEventLoopTimer (GetCurrentEventLoop(), 0, 0.01, sTimerUPP, (void *) window, &timer);

   einfo->info.drawable = window;

   evas_engine_info_set(evas, (Evas_Engine_Info *) einfo);

   setup();

   orig_start_time = start_time = get_time();

   RunApplicationEventLoop();
   
   evas_shutdown();
   return 0;
}
