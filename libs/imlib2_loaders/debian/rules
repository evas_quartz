#!/usr/bin/make -f
# Sample debian/rules that uses debhelper. 
# GNU copyright 1997 by Joey Hess.
#
# This version is for a hypothetical package that builds an
# architecture-dependant package, as well as an architecture-independent
# package.

# Uncomment this to turn on verbose mode. 
#export DH_VERBOSE=1

# This is the debhelper compatability version to use.
export DH_COMPAT=4


# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

INSTALL=/usr/bin/install -p
CONFIGUREOPTS = --prefix=/usr --build=$(DEB_BUILD_GNU_TYPE) --host=$(DEB_HOST_GNU_TYPE)
package=imlib2-loaders

configure: configure-stamp
configure-stamp:
	dh_testdir

	test -x autogen.sh && ./autogen.sh $(CONFIGUREOPTS) || ./configure $(CONFIGUREOPTS)

	touch configure-stamp

build: configure build-stamp
build-stamp:
	dh_testdir

	$(MAKE) 

	touch build-stamp

clean:
	dh_testdir
	rm -f build-stamp configure-stamp

	-mv debian/changelog debian/changelog.saved
	-$(MAKE) distclean
	-mv debian/changelog.saved debian/changelog

	dh_clean

install: build 
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	$(MAKE) install DESTDIR=$(CURDIR)/debian/$(package)/

binary-indep: build install

binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdocs AUTHORS README
	dh_installchangelogs
	dh_strip
	dh_compress
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
