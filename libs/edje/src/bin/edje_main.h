#ifndef EDJE_MAIN_H
#define EDJE_MAIN_H


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <fcntl.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/mman.h>

#include <Ecore_Evas.h>

#include "edje_private.h"
#include "edje_prefix.h"

#endif
