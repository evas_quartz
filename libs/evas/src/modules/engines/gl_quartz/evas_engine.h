#ifndef EVAS_ENGINE_H
#define EVAS_ENGINE_H

#include <Carbon/Carbon.h>
#include <OpenGL/OpenGL.h>
#include <AGL/agl.h>

#include "evas_gl_common.h"

typedef struct _Evas_GL_Quartz_Window Evas_GL_Quartz_Window;

struct _Evas_GL_Quartz_Window
{
   WindowRef            win;
   int                  w, h;
   AGLContext           context;
   Evas_GL_Context      *gl_context;
   
   struct
   {
      int               redraw : 1;
      int               drew : 1;
      int               x1, y1, x2, y2;
   } draw;
};

extern GLint          _evas_gl_quartz_configuration[5];

Evas_GL_Quartz_Window *
  eng_window_new(WindowRef   win,
		 int      w,
		 int      h);
void
  eng_window_free(Evas_GL_Quartz_Window *gw);
void
  eng_window_use(Evas_GL_Quartz_Window *gw);

#endif
