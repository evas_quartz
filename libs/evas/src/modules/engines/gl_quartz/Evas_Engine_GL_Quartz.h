#ifndef _EVAS_ENGINE_GL_QUARTZ_H
#define _EVAS_ENGINE_GL_QUARTZ_H

#include <Carbon/Carbon.h>
#include <OpenGL/OpenGL.h>
#include <AGL/agl.h>

typedef struct _Evas_Engine_Info_GL_Quartz              Evas_Engine_Info_GL_Quartz;

struct _Evas_Engine_Info_GL_Quartz
{
   /* PRIVATE - don't mess with this baby or evas will poke its tongue out */
   /* at you and make nasty noises */
   Evas_Engine_Info magic;

   /* engine specific data & parameters it needs to set up */
   struct {
		WindowRef  drawable;
   } info;

   struct {
   } func;
};
#endif


