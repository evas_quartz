#include <config.h>

#include "Epdf.h"


const char *
epdf_poppler_version_get (void)
{
  return POPPLER_VERSION;
}
