#ifndef _EDJE_EDITOR_SCRIPT_H_
#define _EDJE_EDITOR_SCRIPT_H_


/* script frame objects */
Etk_Widget *UI_ScriptBox;
Etk_Widget *UI_ScriptSaveButton;


Etk_Widget* script_frame_create (void);
void        script_frame_update (void);


#endif
