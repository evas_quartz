#ifndef __E_IPC_H
#define __E_IPC_H

#define IPC_TITLE "entrance_ipc"

#define E_XAUTH_REQ  0xA0
#define E_XAUTH_ACK  0xA1
#define E_XAUTH_NAK  0xA2
#define E_UID        0x10
#define E_GID        0x11
#define E_HOMEDIR    0x12

#endif
