/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_STOLEN_H
#define E_STOLEN_H

EAPI int  e_stolen_win_get(Ecore_X_Window win);
EAPI void e_stolen_win_add(Ecore_X_Window win);
EAPI void e_stolen_win_del(Ecore_X_Window win);

#endif
#endif
