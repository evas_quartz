/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_TEST_H
#define E_TEST_H

EAPI void e_test(void);

#endif
#endif
