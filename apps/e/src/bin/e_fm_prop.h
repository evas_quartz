/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_FM_PROP_H
#define E_FM_PROP_H

EAPI E_Config_Dialog *e_fm_prop_file(E_Container *con, E_Fm2_Icon *ic);

#endif
#endif
