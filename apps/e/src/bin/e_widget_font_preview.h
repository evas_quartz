/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_WIDGET_FONT_PREVIEW_H
#define E_WIDGET_FONT_PREVIEW_H

EAPI Evas_Object     *e_widget_font_preview_add(Evas *evas, const char *text);
EAPI void             e_widget_font_preview_font_set(Evas_Object *obj, const char *font, Evas_Font_Size size);
    
#endif
#endif
