/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_INT_BORDER_PROP_H
#define E_INT_BORDER_PROP_H

EAPI void e_int_border_prop(E_Border *bd);

#endif
#endif
