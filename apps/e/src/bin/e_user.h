/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_USER_H
#define E_USER_H

EAPI const char *e_user_homedir_get(void);
EAPI const char *e_user_desktop_dir_get(void);
EAPI const char *e_user_icon_dir_get(void);

#endif
#endif
