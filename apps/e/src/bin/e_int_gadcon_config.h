/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_INT_GADCON_CONFIG_H
#define E_INT_GADCON_CONFIG_H

EAPI void e_int_gadcon_config_shelf   (E_Gadcon *gc);
EAPI void e_int_gadcon_config_toolbar (E_Gadcon *gc);

#endif
#endif
