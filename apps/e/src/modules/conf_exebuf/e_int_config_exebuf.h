#ifdef E_TYPEDEFS
#else
#ifndef E_INT_CONFIG_EXEBUF_H
#define E_INT_CONFIG_EXEBUF_H

EAPI E_Config_Dialog *e_int_config_exebuf(E_Container *con, const char *params __UNUSED__);

#endif
#endif
