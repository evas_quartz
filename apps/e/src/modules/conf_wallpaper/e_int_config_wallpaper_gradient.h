#ifdef E_TYPEDEFS
#else
#ifndef E_INT_CONFIG_WALLPAPER_GRADIENT_H
#define E_INT_CONFIG_WALLPAPER_GRADIENT_H

EAPI E_Dialog *e_int_config_wallpaper_gradient(E_Config_Dialog *parent);
EAPI void      e_int_config_wallpaper_gradient_del(E_Dialog *dia);
    
#endif
#endif
