#ifdef E_TYPEDEFS
#else
#ifndef E_INT_CONFIG_BORDERS_H
#define E_INT_CONFIG_BORDERS_H

EAPI E_Config_Dialog *e_int_config_borders(E_Container *con, const char *params __UNUSED__);
EAPI E_Config_Dialog *e_int_config_borders_border(E_Container *con, const char *params);

#endif
#endif
