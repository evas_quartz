/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_INT_CONFIG_FONTS_H
#define E_INT_CONFIG_FONTS_H

EAPI E_Config_Dialog *e_int_config_fonts(E_Container *con, const char *params __UNUSED__);

#endif
#endif
