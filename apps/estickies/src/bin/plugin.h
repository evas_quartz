#ifndef _PLUGIN_H
#define _PLUGIN_H

int _e_plugin_init();
int _e_plugin_shutdown();

int _e_plugin_load();
int _e_plugin_unload();

#endif
