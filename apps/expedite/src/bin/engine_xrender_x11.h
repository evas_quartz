#ifndef ENGINE_XRENDER_X11_H
#define ENGINE_XRENDER_X11_H

int  engine_xrender_x11_args(int argc, char **argv);
void engine_xrender_x11_loop(void);

#endif
