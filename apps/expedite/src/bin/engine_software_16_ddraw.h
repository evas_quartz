#ifndef __ENGINE_SOFTWARE_16_DDRAW_H__
#define __ENGINE_SOFTWARE_16_DDRAW_H__


#ifdef __cplusplus
extern "C" {
#endif


int  engine_software_16_ddraw_args(int argc, char **argv);
void engine_software_16_ddraw_loop(void);


#ifdef __cplusplus
}
#endif


#endif /* __ENGINE_SOFTWARE_16_DDRAW_H__ */
