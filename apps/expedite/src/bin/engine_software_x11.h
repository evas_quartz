#ifndef ENGINE_SOFTWARE_X11_H
#define ENGINE_SOFTWARE_X11_H

int  engine_software_x11_args(int argc, char **argv);
void engine_software_x11_loop(void);

#endif
