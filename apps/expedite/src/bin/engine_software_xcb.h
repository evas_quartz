#ifndef ENGINE_SOFTWARE_XCB_H
#define ENGINE_SOFTWARE_XCB_H

int  engine_software_xcb_args(int argc, char **argv);
void engine_software_xcb_loop(void);

#endif
