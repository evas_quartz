#ifndef ENGINE_SOFTWARE_SDL_H
#define ENGINE_SOFTWARE_SDL_H

int  engine_software_sdl_args(int argc, char **argv);
void engine_software_sdl_loop(void);

#endif
