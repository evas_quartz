#ifndef ENGINE_GL_X11_H
#define ENGINE_GL_X11_H

int  engine_gl_x11_args(int argc, char **argv);
void engine_gl_x11_loop(void);

#endif
