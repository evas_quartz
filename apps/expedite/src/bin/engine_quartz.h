#ifndef ENGINE_QUARTZ_H
#define ENGINE_QUARTZ_H

int  engine_quartz_args(int argc, char **argv);
void engine_quartz_loop(void);

#endif
