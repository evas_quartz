#ifndef __ENGINE_DIRECT3D_H__
#define __ENGINE_DIRECT3D_H__


#ifdef __cplusplus
extern "C" {
#endif


int  engine_direct3d_args(int argc, char **argv);
void engine_direct3d_loop(void);


#ifdef __cplusplus
}
#endif


#endif /* __ENGINE_DIRECT3D_H__ */
