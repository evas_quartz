#ifndef ENGINE_DIRECTFB_H
#define ENGINE_DIRECTFB_H

int  engine_directfb_args(int argc, char **argv);
void engine_directfb_loop(void);

#endif
